#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import os
import statistics
reload(statistics)

from scipy import misc
from statistics import Statistics

class Character(Statistics):
	def __init__(self,train_pathname,name='obj'):
		self.name = name.split('_')[0]
		self.data = {}
		self.train_pathname = train_pathname
		self.filenames = self.find_corresponding_filenames()
		self.features = {'mux':np.array([]),
								'muy':np.array([]),
								'sigmax':np.array([]),
								'sigmay':np.array([]),
								'PCA':{},
								'skewx':np.array([]),
								'skewy':np.array([]),
								'modeX':np.array([]),
								'modeY':np.array([])}
		self.pdf = {'p2D':{},
						'px':np.array([]),
						'py':np.array([])}
		
	def find_corresponding_filenames(self):
		tmpName = []
		for item in os.listdir(self.train_pathname):
			if self.name == item.split('_')[0]:
				tmpName.append(item)
		return tmpName
				
	def rgb2gray(self,rgb):
		r, g, b = rgb[:,:,0], rgb[:,:,1], rgb[:,:,2]
		gray = 0.2989 * r + 0.5870 * g + 0.1140 * b
		return gray
		
	def pixel_cleaning(self,data,threshold = 180):
		data[data>threshold] = 255
		return data
		
	def pixel_invert(self,data):
		return 255-data
		
	def plot(self,data,xdata='',style = 'character'):
		'''
		To plot the files we have loaded or their treated versions
		style:
			character - to plot the characters themselves as images
			statistics - to plot 2D statistics of the characters
		'''
		
		N = len(data)
		if N>16:
			N = 16
		row = np.round(np.sqrt(N))
		if style == 'character':
			plt.figure(1)
			count = 1
			for i in data.keys():
				if len(data[i].shape) < 2:
					print 'Wrong data type for plotting!'
				else:
					plt.subplot(row,row,count)
					plt.imshow(data[i])
					count += 1
			plt.gray()
		else:
			plt.figure(1)
			count = 1
			for i in np.arange(data.shape[0]):
				if xdata == '':
					xdata = np.arange(data.shape[1])
				if len(data.shape)<2:
					plt.scatter(xdata,data)
				else:
					ax=plt.subplot(row,row,count)
					plt.scatter(xdata,data[i,:])
					ax.annotate(count, xy=(0.8, 0.8), xycoords='axes fraction')
				count += 1
		plt.show()
		
	def loadData(self):
		count = 1
		for item in self.filenames:
			self.data[count]=misc.imread(os.path.join(self.train_pathname,item))	
			count += 1
		
class TrainCharacter(Character):
	pass


	
class TestCharacter(Character):
	pass


if __name__ == "__main__":
	print 'hello'
