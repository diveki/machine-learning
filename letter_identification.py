#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy as sc

from scipy import misc

<<<<<<< HEAD:letter_identification.py
def rgb2grey(x):
	return x.mean(2)
	
def find_borders(x,direction='vertical',threshold=250):
	if direction == 'vertical':
		tv = np.square(np.diff(x,1,0))
		tvs = tv.sum(0)
		tresh = tvs.mean()-tvs.std()
		if tresh < 0:
			tresh = tvs.mean()
		idx = border_selection(np.where(tvs<tresh))
		return idx
	else:
		td = np.square(np.diff(x,1,1))
		tt = np.where(td.sum(1)<threshold)
		idx = border_selection(tt)
		idx_diff = idx[1:]-idx[:-1]
		imean = idx_diff[:-1].mean()
		np.put(idx,-1,np.ceil(idx[-2]+imean))
		return idx

def border_selection(x):
	tdiff = x[0][1:]-x[0][:-1]
	idx = np.where(tdiff==1)
	x[0][idx[0]]=0
	vborder = np.delete(x[0],np.where(x[0]==0))
	vdiff = vborder[1:]-vborder[:-1]
	vlim = vdiff.mean()+1.5*vdiff.std()
	idx_vlim = np.where(vdiff>vlim)
	vidx = np.insert(vborder,idx_vlim[0]+1,vborder[idx_vlim[0]]+vdiff.mean().round())
	return vidx
		
def letter_identification(tg,thres=180):
	tg[tg<=thres]=0
	idxH = find_borders(tg,direction='horizontal')
	idxV=[]
	for i in range(len(idxH)-1):
		tmp = find_borders(tg[idxH[i]+1:idxH[i+1]],direction='vertical')
		idxV.append(tmp)
	return idxH, idxV
	
if __name__ == "__main__":
	t = misc.imread('test.png')
	tg = rgb2grey(t)
	idxH, idxV = letter_identification(tg)
	tg[idxH,:]=100
	for i in range(len(idxH)-1):
		tg[idxH[i]+1:idxH[i+1],idxV[i]]=100

	plt.imshow(tg)
	plt.gray()
	plt.show()

	print "Hello ocr!"
=======
# to be implemented
>>>>>>> 4f0186c49fc7fc6f8f731e24688bfe377bd8579a:ocr.py
