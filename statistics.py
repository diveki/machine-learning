# -*- coding: iso-8859-15 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import os
import sklearn as sk

from scipy import misc

class Statistics:
	def probability(self,data,ptype='All'):
		norm = np.sum(np.sum(data))
		if ptype == 'All':
			p2d = data/norm
			px = np.sum(data,0)/norm
			py = np.sum(data,1)/norm
			return p2d, px, py
	def mean(self,data,xdata=''):
		if xdata == '':
			xdata = np.arange(len(data))
		return np.sum(xdata*data)
		
	def variance(self,data,xdata=''):
		if xdata == '':
			xdata = np.arange(len(data))
		ex2 = np.square(np.sum(xdata*data))
		e2x = np.sum(np.square(xdata)*data)
		return e2x-ex2
	
	def skew(self,data,xdata=''):
		if xdata == '':
			xdata = np.arange(len(data))
		ex = np.sum(xdata*data)
		return np.sum((xdata-ex)**3*data)
		
	def find_peaks(self,data,relPeakThr=0.25,relValueThr=0.1):
		d = np.diff(data)
		idx = []
		dmax = np.max(data)
		for i in np.arange(len(d)-1):
			if d[i]>=0 and d[i+1]<0 and data[i+1]/dmax > relPeakThr and i > 0 and d[i-1] > 0:
				idx.append((i+1,data[i+1]))
		return idx
