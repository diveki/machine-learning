#!/usr/bin/python
# -*- coding: iso-8859-15 -*-

import numpy as np
import matplotlib.pyplot as plt
import scipy as sc
import os

from scipy import misc

class TrainingSet():
	pathname = os.path.abspath('Letters')
	def __init__(self,path=pathname):
		self.pathname = path

	def create_labels(self):
		self.filename = os.listdir(self.pathname)
		tmp={}
		for i in range(len(self.filename)):
			k = self.filename[i].split('.')[0].split('_')[0]
			if tmp.has_key(k):
				tmp[k] += 1
			else:
				tmp[k] = 1
		self.labels = tmp
		return tmp
		
	def load_training_set(self,path=pathname,imname='',tr_ratio=0.7):
		self.train = {}
		self.test = {}
		for key in self.labels.keys():
			N = np.round(self.labels[key]*tr_ratio)
			count = 0
			self.train[key]={}
			self.test[key]={}
			for item in self.filename:
				if key == item.split('_')[0]:
					count += 1
					if  count <= N:
						self.train[key][count]=misc.imread(os.path.join(self.pathname,item))
					else:
						self.test[key][count]=misc.imread(os.path.join(self.pathname,item))

		
if __name__ == "__main__":
	a = TrainingSet()
	a.create_labels()
	a.load_training_set(imname=a.labels)

#	plt.imshow(tg)
#	plt.gray()
#	plt.show()

	print "Hello ocr!"
